from __future__ import annotations
from typing import List, Union, Tuple, Dict, Optional
import subprocess
from builtins import dict
from win32com.client import Dispatch, GetObject, GetActiveObject
import pythoncom
import pywintypes
import traceback
# import kustom_widgets as kw
import collections
import numpy as np

"""See module_tests/excel_csv_tools.py for reading and writing to csv_files"""


def put_data(list_like, inp_address=None, T=False, safe_put=True):
    """ Pastes list, dict, or OrderedDict as data
        if inp_address is blank, the cursor is used, alternately us a string such as a3
    set T=True to transpose data"""
    ex_cls = ActiveExcel()
    if ex_cls.excel_working:
        return ex_cls.put_data(list_like, inp_address, T, safe_put=safe_put)
    else:
        _com_error()


def get_data(address: str = None, as_numpy: bool = True, all_data: bool = False):
    """Puts selected data into numpy matrix, if as_numpy set to False, list of lists,
    Use get_data_and_headers for data with headers"""
    ex_cls = ActiveExcel()
    if ex_cls.excel_working:
        if address or all_data:
            raw_data = ex_cls.get_data(address=address, all_data=all_data)
        else:
            raw_data = ex_cls.get_sel()
        if not as_numpy:
            return raw_data
        data = np.array(raw_data)
        if len(data.shape) > 0:
            if data.shape[1] == 1:
                return data.transpose()[0]
            return data
        else:
            try:
                return data[0]
            except IndexError:
                return data
    else:
        _com_error()


def get_data_and_headers(remove_empty_cells=False, set_type: Optional[any] = None):
    """Puts selected data into numpy matrix,
    Use get_data_and_headers for data with headers"""
    ex_cls = ActiveExcel()
    if ex_cls.excel_working:
        raw_data = list(ex_cls.get_sel())
        headers = raw_data.pop(0)
        if not set_type:
            set_type = object
        data_list = [data for data in np.array(raw_data, dtype=set_type).transpose()]
        od = {}
        for i, header in enumerate(headers):
            if header in od:
                print('Warning: header ' + str(header) + ' already exists, will be overwritten')
            if remove_empty_cells:
                x = data_list[i]
                od[header] = x[x != np.array(None)]
            else:
                if len(data_list[i]) > 1:
                    od[header] = data_list[i]
                else:
                    od[header] = data_list[i][0]
        return od
    else:
        _com_error()


def paste_header_data(list_like, inp_address=None, safe_put=True):
    """ Pastes list, dict, or OrderedDict as data with first row being the header
    if inp_address is blank, the cursor is used, alternately us a string such as a3 """
    ex_cls = ActiveExcel()
    if ex_cls.excel_working:
        return ex_cls.paste_header_data(list_like, inp_address, safe_put=safe_put)
    else:
        _com_error()


def get_cursor_address() -> str:
    return ActiveExcel().excel.selection.address


def insert_worksheet(sheet_name=None, select=True, overwrite=True):
    ex_cls = ActiveExcel()
    if ex_cls.excel_working:
        # sheet_name = ex_cls.get_next_available_sheet_name(sheet_name, overwrite)
        ex_cls.insert_worksheet(sheet_name, overwrite)
        if select:
            activate_worksheet(sheet_name)
        return ex_cls
    else:
        _com_error()


def get_tab_name():
    ex_cls = ActiveExcel()
    if ex_cls.excel_working:
        return ex_cls.get_sheet_info_dict()['Sheet name']
    else:
        _com_error()


def insert_image(image_path: str, xy_location=None, resize_height: Union[bool, int] = False):
    ex_cls = ActiveExcel()
    if ex_cls.excel_working:
        ex_cls.insert_image(image_path, xy_location, resize_height)
    else:
        _com_error()


def activate_worksheet(sheet):
    ex_cls = ActiveExcel()
    if ex_cls.excel_working:
        ex_cls.activate_sheet(sheet)
    else:
        _com_error()


def _com_error():
    print('COM port for Excel does not seem to be working')


def screen_updating_off():
    ex_cls = ActiveExcel()
    ex_cls.screen_updating(False)


def screen_updating_on():
    ex_cls = ActiveExcel()
    ex_cls.screen_updating(True)


def exterminate_all_excel_instances():
    """Use when there is a lost instance of excel running, everything will shutdown without option to save"""
    #  equivalent to: taskkill /F /IM EXCEL.EXE
    args = ['taskkill', '/F', '/IM', 'EXCEL.EXE']
    subprocess.Popen(args)


def get_dispatch_obj():
    return Dispatch('Excel.Application')


class ActiveExcel(object):

    def __init__(self, wb_name: str = None, sheet: str = None, suppress_error=False):
        # self.excel = Dispatch('Excel.Application')  # get a handle to the Excel COM object
        self.excel = self._get_com()  # get a handle to the Excel COM object
        self.set_excel_constants()
        self.excel_working = False
        self.sheet_specified = None
        try:
            if self.excel.Workbooks.Count > 0:
                # if wb_name:
                self._set_working(sheet)
            else:
                self._open_default_workbook(sheet)
        except AttributeError as e:
            if not suppress_error:
                tb = traceback.format_exc()
                print(f'error: {e} with: \n{tb}')

    def _get_com(self):
        try:
            return GetObject('Excel.Application')
        except pythoncom.com_error as error:
            return Dispatch('Excel.Application')

    def _open_default_workbook(self, sheet):
        if not self.is_wb_open('Book1', select=True):  # if not the correct name, add new wb
            self.excel.Workbooks.Add()
            if self.excel.Workbooks.Count > 0:
                self._set_working(sheet)

    def _set_working(self, sheet):
        self.excel_working = True
        self.sheet_specified = sheet
        self.excel.Visible = True
        if sheet:
            self.activate_sheet(sheet)

    def activate_sheet(self, sheet):
        self.live_wb.Worksheets(sheet).Activate()
        try:
            self.live_wb.Worksheets(sheet).Activate()
        except Exception as e:
            print('Failed to activate sheet. ' + str(e))

    @property
    def live_wb(self):
        if self.excel.Workbooks.Count < 1:
            return False
        try:
            return self.excel.ActiveWorkbook  # .Add(filename)
        except Exception as e:  # gets last workbook, note that count starts at 1
            print('This except type needs to be determined!: ' + str(e))
            return self.excel.Workbooks(self.excel.Workbooks.Count)

    @property
    def live_ws(self):
        if self.sheet_specified is None:
            return self.live_wb.ActiveSheet
        else:
            return self.live_wb.Worksheets(self.sheet_specified)

    def get_sel(self):
        return self.excel.selection.Value

    @property
    def selection(self):
        return self.get_sel()

    def put_data(self, list_like, inp_address=None, T=False, safe_put=False):
        """ Pastes list, dict, or OrderedDict as data
        if inp_address is blank, the cursor is used, alternately us a string such as a3,
        safe_put will return the wb_name, sheet and address if there is data in the cells """
        # self = cls()
        if not inp_address:
            inp_address = self.excel.selection.address
        new_address = XlAddress.from_alpha(inp_address)
        if isinstance(list_like, str):
            list_like = [[list_like]]
        elif is_dicty(list_like):
            list_like = convert_OrderedDict(list_like, True)
        elif is_listy(list_like):
            list_like = convert_list(list_like)
        self._insert_data(list_like, new_address, T, safe_put)
        # if T:
        #     list_like = transpose_list(list_like)
        # # list_like = check_contiguous(list_like)
        # list_dims = XlAddress.from_data(list_like)
        # list_dims.offset_range(new_address.start-(1, 1))
        # if safe_put:
        #     if not self.are_cells_empty(list_dims.address):
        #         print(self.are_cells_empty(list_dims.address))
        #         return self.get_sheet_info_dict(list_dims.address)
        # self.live_ws.Range(list_dims.address).Value = list_like

    def paste_header_data(self, list_like, inp_address=None, T=False, safe_put=False):
        """ Pastes list, dict, or OrderedDict as data with first row being the header
        if inp_address is blank, the cursor is used, alternately us a string such as a3 """
        address = inp_address
        if not inp_address:
            address = self.excel.selection.address
        # self.live_ws = self.live_wb.ActiveSheet
        new_address = XlAddress.from_alpha(address)
        if isinstance(list_like, collections.OrderedDict) or isinstance(list_like, dict):
            list_like = convert_OrderedDict(list_like)
        self._insert_data(list_like, new_address, T, safe_put)
        # list_dims = XlAddress.from_data(list_like)
        # list_dims.offset_range(new_address.start-(1, 1))
        # if safe_put:
        #     if not self.are_cells_empty(list_dims.address):
        #         return self.get_sheet_info_dict(list_dims.address)
        # self.live_ws.Range(list_dims.address).Value = list_like

    def _insert_data(self, list_like, new_address: XlAddress, T: bool, safe_put: bool):
        if T:
            list_like = transpose_list(list_like)
        # list_like = check_contiguous(list_like)
        list_dims = XlAddress.from_data(list_like)
        list_dims.offset_range(new_address.start-(1, 1))
        if safe_put:
            if not self.are_cells_empty(list_dims.address):
                print(f'Cells are not empty, set safe put to False to overwrite')
                return self.get_sheet_info_dict(list_dims.address)
        self.live_ws.Range(list_dims.address).Value = list_like

    def insert_worksheet(self, sheet_name=None, overwrite=False):
        """Inserts a worksheet after the current selection,
        use sheet_name to specify name, None will use next available name"""
        sheet_name = self.get_next_available_sheet_name(sheet_name,overwrite=overwrite)
        new_worksheet = self.live_wb.Worksheets.Add()
        if sheet_name is not None:
            new_worksheet.Name = sheet_name
        return new_worksheet

    def get_data(self, address=None, all_data: bool = False):
        if address is None and all_data is None:
            return self.get_sel()
        if all_data:
            result = self.live_ws.UsedRange.Value
        else:
            addr_obj = XlAddress.from_alpha(address)
            result = self.live_ws.Range(addr_obj.address).Value
        if isinstance(result, tuple):
            if len(result) > 1 and len(result[0]) == 1:  # multi_row, single column
                return tuple([r[0] for r in result])
            else:
                return result
        return result[0]

    def get_headers(self, inp_address=None):
        if not inp_address:
            inp_address = self.excel.selection.address
        address = XlAddress.from_alpha(inp_address)
        last_col = self.last_column_in_row(address.bounds[0])
        addr_obj = XlAddress.from_tuple(((address.bounds[0]), (last_col, address.bounds[1][1])))
        headers = self.live_ws.Range(addr_obj.address).Value[0]
        return headers

    def get_cells(self, inp_address=None):
        """gets the cells selected or at the address which can be a range"""
        if not inp_address:
            inp_address = self.excel.selection.address
        address = XlAddress.from_alpha(inp_address)
        addr_obj = XlAddress.from_tuple(address.bounds)
        cell_range = self.excel.Range(addr_obj.address)
        return cell_range.Value

    def last_column_in_row(self, address):
        count = self.live_ws.Columns.Count
        big_num = self.live_ws.Cells(1, count).End(self.xl_cons['xlToRight']).Column
        max_gap = 5
        start_address = XlAddress.from_tuple((address, (big_num, address[1])))
        row_data = self.live_ws.Range(start_address.address).Value
        counter = 0
        for cell in enumerate(row_data[0]):
            if cell[1] is None:
                counter += 1
            else:
                counter = 0
            if counter >= max_gap:
                break
        return cell[0] - max_gap + 2

    def set_excel_constants(self):
        self.xl_cons = {
                        'xlToLeft': 1,
                        'xlToRight': 2,
                        'xlUp': 3,
                        'xlDown': 4,
                        'xlThick': 4,
                        'xlThin': 2,
                        'xlEdgeBottom': 9,
                        'xlShiftDown': -4121,
                        }

    def add_sheet(self, sheet_name, overwrite=False):
        if overwrite:
            sheet_name = self.get_next_available_sheet_name(sheet_name, overwrite=True)
        else:
            if self.get_sheet_by_name(sheet_name, select=True) is not None:
                print('Sheet name already exists, overwrite not selected')
                return
        self.live_wb.Sheets.Add().Name = sheet_name

    def delete_sheet(self, sheet_name):
        cur_sheet = self.get_sheet_by_name(sheet_name)
        if cur_sheet is not None and self.live_wb.Sheets.Count > 1:
            cur_sheet.Delete()

    def get_sheet_by_name(self, sheet_name, select=False):
        for i in range(1, self.live_wb.Sheets.count+1):
            if self.live_wb.Sheets(i).Name == sheet_name:
                if select:
                    self.live_wb.Sheets(i).Activate()
                return self.live_wb.Sheets(i)

    def get_next_available_sheet_name(self, sheet_name, overwrite=False):
        """Use when auto-generating sheets and names"""
        sheet_names = [self.live_wb.Sheets(i).Name for i in range(1, self.live_wb.Sheets.count+1)]
        if sheet_name in sheet_names:
            for i in range(1, self.live_wb.Sheets.count + 1):
                if overwrite:
                    if self.live_wb.Sheets(i).Name == sheet_name:
                        self.excel.DisplayAlerts = False
                        self.live_wb.Sheets(i).Delete()
                        self.excel.DisplayAlerts = True
                        return sheet_name
                else:
                    new_name = sheet_name + '_' + str(i)
                    if new_name not in sheet_names:
                        return new_name
        else:
            return sheet_name
        print('Error in available sheet names')

    def is_wb_open(self, wb_name, select=False):
        for i in range(self.excel.Workbooks.Count):
            if wb_name == self.excel.Workbooks(i+1).Name:  # Workbooks start at 1
                if select:
                    self.excel.Workbooks(i+1).Activate
                    # self.setup_active()
                return True
        return False

    def save_as(self, filename):
        self.excel.ScreenUpdating = False
        self.excel.DisplayAlerts = False
        filename = filename.replace('/', '\\')
        try:
            self.live_wb.SaveAs(filename)
        except pywintypes.com_error as e:
            print(str(e))
        self.excel.ScreenUpdating = True
        self.excel.DisplayAlerts = True

    def are_cells_empty(self, address):
        try:
            if not np.any(self.get_cells(address)):
                return True
            return False
        except TypeError:  # the exception is that the data contains different types,
            return False   # and confirms that the cells are not empty

    def get_sheet_info_dict(self, address=None):
        l = self.live_wb
        return {'Workbook name': l.name, 'Sheet name': l.ActiveSheet.name,
                'Range': address}

    def insert_image(self, image_path: str, xy_pos: Tuple[int, int], resize_height: Union[bool, int] = False):
        # If no position provided, figure out where top of next image should be.
        if xy_pos is None:
            top = 0
            for shp in self.live_wb.ActiveSheet.Shapes:
                if shp.top + shp.Height > top:
                    top = shp.top + shp.Height
        else:
            top = xy_pos[1]
        image_path = image_path.replace('/', '\\')
        sr = self.live_wb.ActiveSheet.Shapes.AddPicture(image_path, False, True, 0, top, -1, -1)
        sr.LockAspectRatio = True
        if resize_height:
            sr.Height = resize_height
        if xy_pos is None:
            sr.Left, sr.Top = 0, top
        else:
            sr.Left, sr.Top = xy_pos

    def screen_updating(self, is_active: bool = True):
        self.ScreenUpdating = is_active

    def quit(self):
        try:
            self.excel.DisplayAlerts = False
            self.excel.quit()
        except (pythoncom.com_error, AttributeError) as e:
            print(f'error: {e}')


class XlAddress(object):

    def __init__(self):
        pass

    @classmethod
    def from_alpha(cls, address):
        self = cls()
        self.address = address.replace('$', '')
        self.alpha_to_bounds()
        return self

    @classmethod
    # This needs fixing
    def from_data(cls, dlist):
        self = cls()
        try:
            if not is_2d(dlist):
                self.row_count = 1
                self.column_count = len(dlist)
            else:
                self.row_count = len(dlist)
                self.column_count = len(dlist[0])
        except (TypeError, KeyError, IndexError) as e:
            try:
                self.row_count = 1
                self.column_count = len(dlist)
            except (TypeError, KeyError, IndexError) as e:
                    self.row_count = 1
                    self.column_count = 1
        if self.column_count == 0:
            self.column_count = 1
        self.address = ('A1:' + (num2alpha(self.column_count).upper()) + str(self.row_count))
        self.alpha_to_bounds()
        return self

    @classmethod
    def from_tuple(cls, double_tuple):
        self = cls()
        self.bounds = double_tuple
        self.bounds_to_points()
        self.bounds_to_alpha()
        return self

    def alpha_to_bounds(self):
        slash_pos = self.address.find(':')
        if slash_pos == -1:
            self.address += ':' + self.address
            slash_pos = self.address.find(':')
        address_tuple = self.address[:slash_pos], self.address[slash_pos+1:]
        self.bounds = []
        for a in address_tuple:
            alpha = ''
            number = ''
            for letter in a:
                if letter.isnumeric():
                    number += letter
                else:
                    alpha += str(letter)
            self.bounds.append((int(alpha2num(alpha)), int(number)))
        self.bounds = np.array(self.bounds)
        self.bounds_to_points()

    def bounds_to_points(self):
        self.address_numeric = self.bounds
        self.start = self.bounds[0]
        self.end = self.bounds[1]
        self.column_count = self.end[0] - self.start[0] + 1
        self.row_count = self.end[1] - self.start[1] + 1

    def offset_range(self, row_col_tuple):
        rc_offset = np.array(row_col_tuple)
        self.bounds[0] = self.bounds[0] + rc_offset
        self.bounds[0][self.bounds[0] < 1] = 1
        self.bounds[1] = self.bounds[0] + (self.column_count-1, self.row_count-1)
        self.bounds_to_points()
        self.bounds_to_alpha()

    def bounds_to_alpha(self):
        self.address = ((num2alpha(self.start[0]).upper()) + str(self.start[1]) +
                        ':' + (num2alpha(self.end[0]).upper()) + str(self.end[1]))

    @property
    def address_upper_left(self):
        """Gives a single address, such as A1"""
        return num2alpha(self.start[0]).upper() + str(self.start[1])


def alpha2num(letters):
    letters = letters.lower()
    abet = alphabet()
    base = len(abet)
    alpha_places = len(letters)
    total = 0
    for place in range(alpha_places):
        cur_letter = letters[alpha_places - place - 1]
        loc = abet.find(cur_letter) + 1
        total += (loc*(base**place))
    return total


def num2alpha(num):
    numerals = alphabet()
    base = len(numerals)
    s = ''
    if num == 0:
        return 0
    num -= 1  # since 0 is 0 and A is 1
    for i in range(20):
        s = numerals[int(num % base)] + s
        num = num / base
        if num < 1:
            break
        num -= 1
    return s


def alphabet():
    return 'abcdefghijklmnopqrstuvwxyz'


def denumpy_dict(indict):
    new_dict = {}
    for k, v in indict.items():
        new_dict[k] = numpy2intlist(v)
    return new_dict


def numpy2intlist(narray):
    #  only for 1d int arrays at the moment
    new_list = []
    # if 'int' in str(narray.dtype):
    for num in narray:
        new_list.append(int(num))
    return new_list


def dict2list(indict, vert=True, nested_value=True):
    new_list = []
    if vert and nested_value:
        for k, v in indict.items():
            new_list.append([k] + v)
    return new_list


def transpose_list(inlist):
    #  for 2d lists
    if is_2d(inlist):
        return list(zip(*inlist))
    return [[l] for l in inlist]
    # return [list(z) for z in zip(*inlist)]


def check_contiguous(list_like):
    try:
        flag = list_like.flags.c_contiguous
    except AttributeError:  # not numpy array
        return list_like
    if not flag:
        print('not contiguous')
        list_like = list_like.copy(order='C')
        print(list_like)
        print(list_like.flags)
        return list_like
    return list_like


def convert_list(some_list, transpose=False):
    if not type(some_list) == np.ndarray and is_2d(some_list):
        new_list = _pad_2d_list(some_list)
    else:
        new_list = denumpy_list(some_list)
    if transpose:
        new_list = transpose_list(new_list)
    return new_list


def _pad_2d_list(some_list: List[List[any]]) -> List[List[any]]:
    max_cols = max([len(row) for row in some_list])
    new_list = []
    for values in some_list:
        try:
            len_diff = max_cols - len(values)
            padded_values = values + [''] * len_diff
            new_list.append(denumpy_list(padded_values))
        except TypeError as e:
            print(e, "\nError in values:")
            print(values)
    return new_list


def convert_OrderedDict(o_dict, transpose=True):
    new_list = [[key] for key in o_dict]
    largest_dim = 0
    for values in o_dict.values():
        if values is not None and hasattr(values, '__len__') and len(values) > largest_dim:
            largest_dim = len(values)
    for i, (key, values) in enumerate(o_dict.items()):
        try:
            new_values = [None] * largest_dim
            if any(isinstance(x, (np.ndarray, np.generic)) for x in values):
                # new_values = [None] * len(values)
                for j, x in enumerate(values):
                    if isinstance(x, (np.ndarray, np.generic)):
                        try:
                            new_values[j] = float(x)
                        except ValueError:
                            new_values[j] = x
            elif values is not None:
                new_values[:len(values)] = values
            new_list[i] += list(new_values)
        except TypeError as e:
            print(e, "\nError in '" + str(key) + "', value:")
            print(values)
    if transpose:
        return transpose_list(new_list)
    return new_list


def denumpy_list(values) -> List[any]:
    if type(values) == np.ndarray:
        return values.tolist()
    print(f'{type(values) = }')
    if any(isinstance(x, (np.ndarray, np.generic)) for x in values):
        new_values = [None] * len(values)
        for j, x in enumerate(values):
            if isinstance(x, (np.ndarray, np.generic)):
                new_values[j] = float(x)
    else:
        new_values = values
    return list(new_values)


def is_2d(list_like):
    try:
        return is_listy(list_like[0])
    except IndexError:
        print(list_like)
        return False


def is_dicty(uobj):
    return is_type(uobj, [dict, collections.OrderedDict])


def is_listy(uobj):
    return is_type(uobj, [list, tuple, np.ndarray])


def is_type(uobj, types):
    for _type in types:
        if isinstance(uobj, _type):
            return True
    return False
