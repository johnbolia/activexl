from __future__ import annotations
from typing import TYPE_CHECKING, Dict, List, Tuple, Union, Optional
from distutils.core import setup

setup(name='ActiveXL',
      version='0.1',
      description='Windows XL Utilities',
      author='John Buckley',
      packages=['activexl'],
      install_requires=['pywin32', 'numpy'],  # <- this is probably not complete, needs testing
     )
