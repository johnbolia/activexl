# ActiveXL #

Python module using a COM port to interact with XL

### What is this repository for? ###

* This is a collection of commands, similar to xlwings
* Windows is required
* Version 0.1


[comment]: <> (### How do I get set up? ###)

[comment]: <> (* Summary of set up)

[comment]: <> (* Configuration)

[comment]: <> (* Dependencies)

[comment]: <> (* Database configuration)

[comment]: <> (* How to run tests)

[comment]: <> (* Deployment instructions)

[comment]: <> (### Contribution guidelines ###)

[comment]: <> (* Writing tests)

[comment]: <> (* Code review)

[comment]: <> (* Other guidelines)

[comment]: <> (### Who do I talk to? ###)

[comment]: <> (* Repo owner or admin)

[comment]: <> (* Other community or team contact)